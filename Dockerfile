FROM zaherg/php-7.1-xdebug-alpine:latest

USER root

RUN apk update && \
    apk add --no-cache yaml-dev autoconf gcc g++ make

RUN apk update && \
    pecl install yaml

COPY php.ini /usr/local/etc/php/conf.d/php-dev.ini
COPY xdebug.ini /usr/local/etc/php/conf.d/xdebug-dev.ini

WORKDIR /var/www
